<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
      protected $table = "products";
    protected $fillable = ["category_id","nama","img","harga","berat"];

     function order(){
    	return $this->hasMany('App\Order','product_id','id');
    }
}
