<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
 
    protected $table = "categories";
    protected $fillable = ["nama_category"];

     function product(){
    	return $this->hasMany('App\Product','category_id','id');
    }

}
