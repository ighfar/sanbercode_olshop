<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use pdf;
use UxWeb\SweetAlert\SweetAlert;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $order = Order::all();
        return view('order.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $this->validate($request,[
            'user_id' => 'required',
            'produk_id' => 'required',
            'ekspedisi' => 'required',
            'total' => 'required',
            'status' => 'required',
       
        ]);
        
        Order::create([
            'user_id' => $request->user_id,
            'produk_id' => $request->produk_id,
             'ekspedisi' => $request->ekspedisi,
             'total' => $request->total,
             'status' => $request->status,          
        
        ]);
                SweetAlert::success('Success Message','Data berhasil disimpan');
 
        return redirect('/Order');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $order = Order::find($id);
        return view('order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $order = Order::find($id);
        return view('order.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
             'user_id' => 'required',
            'produk_id' => 'required',
            'ekspedisi' => 'required',
            'total' => 'required',
            'status' => 'required',
          
        ]);
      

        $order = Order::find($id);
        $order->user_id = $request->user_id;
        $order->produk_id = $request->produk_id;
        $order->ekspedisi = $request->ekspedisi;
        $order->total = $request->total;
        $order->status = $request->status;
        $order->update();
                SweetAlert::success('Success Message','Data berhasil diedit');
        return redirect('/order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $order = Order::find($id);
        $order->delete();
                SweetAlert::success('Success Message','Data berhasil dihapus');
        return redirect('/order');
    }
        public function cetak_pdf()
    {
        $order = Order::all();
 
        $pdf = PDF::loadview('order_pdf',['order'=>$order]);
        return $pdf->download('laporan-order-pdf');
    }
	public function export_excel()
	{
		return Excel::download(new OrderExport, 'order.xlsx');
	}
}
