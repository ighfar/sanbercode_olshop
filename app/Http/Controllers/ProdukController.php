<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use pdf;
use UxWeb\SweetAlert\SweetAlert;
use Maatwebsite\Excel\Facades\Excel;

class ProdukController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $produk = Produk::all();
        return view('Produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('produk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $this->validate($request,[
            'category_id' => 'required',
            'nama' => 'required',
            'img' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'harga' => 'required',
            'berat' => 'required',
       
        ]);
        $img = $request->file('img');
 
	   $nama_file = time()."_".$img->getClientOriginalName();
 
      	// isi dengan nama folder tempat kemana file diupload
	   $tujuan_upload = 'data_file';
	    $file->move($tujuan_upload,$nama_file);
 
        Produk::create([
            'category_id' => $request->category_id,
            'nama' => $request->nama,
             'img' => $request->img,
             'harga' => $request->harga,
             'berat' => $request->berat,          
        
        ]);
                SweetAlert::success('Success Message','Data berhasil disimpan');
 
        return redirect('/Produk');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $produk = Produk::find($id);
        return view('produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $produk = Produk::find($id);
        return view('produk.edit', compact('produk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
             'category_id' => 'required',
            'nama' => 'required',
            'img' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'harga' => 'required',
            'berat' => 'required',
          
        ]);
        $img = $request->file('img');
 
       $nama_file = time()."_".$img->getClientOriginalName();
 
        // isi dengan nama folder tempat kemana file diupload
       $tujuan_upload = 'data_file';
        $file->move($tujuan_upload,$nama_file);

        $produk = Produk::find($id);
        $produk->category_id = $request->category_id;
        $produk->nama = $request->nama;
        $produk->img = $request->img;
        $produk->harga = $request->harga;
        $produk->berat = $request->berat;
        $produk->update();
                SweetAlert::success('Success Message','Data berhasil diedit');
        return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $produk = Produk::find($id);
        $produk->delete();
                SweetAlert::success('Success Message','Data berhasil dihapus');
        return redirect('/produk');
    }
        public function cetak_pdf()
    {
        $produk = Produk::all();
 
        $pdf = PDF::loadview('produk_pdf',['produk'=>$produk]);
        return $pdf->download('laporan-produk-pdf');
    }
        public function export_excel()
    {
        return Excel::download(new ProdukExport, 'produk.xlsx');
    }
}
