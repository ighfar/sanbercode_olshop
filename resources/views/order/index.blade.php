@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Order') }}</h1>

    <!-- Begin Page Content -->
                <div class="container-fluid">

         
<a href="order/create" class="btn btn-primary">Tambah</a>
<a href="order/cetak_pdf" class="btn btn-danger">Laporan PDF</a>
    <a href="order/export_excel" class="btn btn-success">Laporan Excel</a> 
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Id User</th>
                <th scope="col"> Id Produk </th>
                <th scope="col">Ekspedisi</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($order as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->user_id}}</td>
                        <td>{{$value->produk_id}}</td>
                        <td>{{$value->ekspedisi}}</td>
                        <td>{{$value->total}}</td>
                           <td>{{$value->status}}</td>
                        <td>
                            <a href="/order/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/order/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/order/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

                </div>
                <!-- /.container-fluid -->


@endsection
