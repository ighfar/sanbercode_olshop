@extends('layouts.admin')
@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Order') }}</h1>
        <div class="container-fluid">
<div>
    <h2>Tambah Data</h2>
        <form action="/order/store" method="POST">
            @csrf
              <div class="form-group">
                <label for="nama">Id User</label>
                <input type="text" class="form-control" name="user_id" id="user_id" placeholder="Masukkan Id User">
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="nama">Id Produk</label>
                <input type="text" class="form-control" name="produk_id" id="produk_id" placeholder="Masukkan Id Produk">
                @error('produk_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
              <div class="form-group">
                <label for="nama">Ekspedisi</label>
                <input type="text" class="form-control" name="ekspedisi" id="ekspedisi" placeholder="Masukkan Ekspedisi">
                @error('ekspedisi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Total</label>
                <input type="text" class="form-control" name="total" id="total" placeholder="Masukkan Total">
                @error('total')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Status</label>
                <input type="text" class="form-control" name="status" id="status" placeholder="Masukkan Status">
                @error('berat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
         
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
</div>
@endsection