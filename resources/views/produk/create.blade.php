@extends('layouts.admin')
@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Produk') }}</h1>
        <div class="container-fluid">
<div>
    <h2>Tambah Data</h2>
        <form action="/produk/store" method="POST" enctype="multipart/form-data">
            @csrf
              <div class="form-group">
                <label for="nama">Id Kategori</label>
                <input type="text" class="form-control" name="category_id" id="category_id" placeholder="Masukkan Id Kategori">
                @error('category_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
              <div class="form-group">
                <label for="nama">Gambar</label>
                <input type="file" class="form-control" name="img" id="img" placeholder="Masukkan Gambar">
                @error('img')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Harga</label>
                <input type="text" class="form-control" name="harga" id="harga" placeholder="Masukkan Harga">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Berat</label>
                <input type="text" class="form-control" name="berat" id="berat" placeholder="Masukkan Berat">
                @error('berat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
         
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
</div>
@endsection