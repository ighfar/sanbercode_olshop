@extends('layouts.admin')
@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Produk') }}</h1>
        <div class="container-fluid">
<div>
     <h2>Edit Produk {{$produk->id}}</h2>
        <form action="/produk/{{$produk->id}}" method="POST">
            @csrf
            @method('PUT')
                     <div class="form-group">
                <label for="nama">Id Kategori</label>
                <input type="text" class="form-control" name="category_id" value="{{$produk->category_id}}" id="category_id" placeholder="Masukkan Id Kategori">
                @error('category_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$produk->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
              <div class="form-group">
                <label for="nama">Gambar</label>
                <input type="file" class="form-control" name="img" value="{{$produk->img}}" id="img" placeholder="Masukkan Gambar">
                @error('img')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Harga</label>
                <input type="text" class="form-control" name="harga" value="{{$produk->harga}}" id="harga" placeholder="Masukkan Harga">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
           <div class="form-group">
                <label for="nama">Berat</label>
                <input type="text" class="form-control" name="berat" value="{{$produk->berat}}" id="berat" placeholder="Masukkan Berat">
                @error('berat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
</div>
@endsection