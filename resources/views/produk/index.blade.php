@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Produk') }}</h1>

    <!-- Begin Page Content -->
                <div class="container-fluid">

         
<a href="produk/create" class="btn btn-primary">Tambah</a>
<a href="produk/cetak_pdf" class="btn btn-danger">Laporan PDF</a>

    <a href="produk/export_excel" class="btn btn-success">Laporan Excel</a> 
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Id Kategori</th>
                <th scope="col">Nama</th>
                <th scope="col">Gambar</th>
                <th scope="col">Berat</th>
                <th scope="col">Harga</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($produk as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->category_id}}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->img}}</td>
                        <td>{{$value->berat}}</td>
                           <td>{{$value->harga}}</td>
                        <td>
                            <a href="/produk/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/produk/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/produk/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

                </div>
                <!-- /.container-fluid -->


@endsection
