<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('kategori', 'KategoriController@index')->name('kategori');
Route::get('kategori/create', 'KategoriController@create');
Route::post('kategori/store', 'KategoriController@store');
Route::get('kategori/{id}', 'KategoriController@show');
Route::get('kategori/{id}/edit', 'KategoriController@edit');
Route::put('kategori/{id}', 'KategoriController@update');
Route::delete('kategori/{id}', 'KategoriController@destroy');
Route::get('kategori/cetak_pdf', 'KategoriController@cetak_pdf');

Route::get('produk', 'ProdukController@index')->name('produk');
Route::get('produk/create', 'ProdukController@create');
Route::post('produk/store', 'ProdukController@store');
Route::get('produk/{id}', 'ProdukController@show');
Route::get('produk/{id}/edit', 'ProdukController@edit');
Route::put('produk/{id}', 'ProdukController@update');
Route::delete('produk/{id}', 'ProdukController@destroy');
Route::get('produk/cetak_pdf', 'ProdukController@cetak_pdf');
Route::get('produk/export_excel', 'ProdukController@export_excel');

Route::get('order', 'OrderController@index')->name('order');
Route::get('order/create', 'OrderController@create');
Route::post('order/store', 'OrderController@store');
Route::get('order/{id}', 'OrderController@show');
Route::get('order/{id}/edit', 'ProdukController@edit');
Route::put('order/{id}', 'OrderController@update');
Route::delete('order/{id}', 'OrderController@destroy');
Route::get('order/cetak_pdf', 'OrderController@cetak_pdf');
Route::get('order/export_excel', 'OrderController@export_excel');

